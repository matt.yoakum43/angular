import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

const BASE_URL = 'http://localhost:3000'

@Injectable({
  providedIn: 'root'
})
export class CoursesService {
  model = 'courses';

  constructor(private http: HttpClient) { }

  all() {
    return this.http.get(this.getUrl());
  }

  find(id) {
    return this.http.get(this.getUrlForId(id));
  }

  create(course) {
    return this.http.post(this.getUrl(), course);
  }

  update(course) {
    return this.http.put(this.getUrlForId(course.id), course);
  }

  delete(id) {
    return this.http.delete(this.getUrlForId(id));
  }



  private getUrl() {
    return `${BASE_URL}/${this.model}`;
  }

  private getUrlForId(id) {
    return `${this.getUrl()}/${id}`;
  }
}
