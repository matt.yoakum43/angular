import { Injectable } from '@angular/core';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  users: User[] = [
    { username: 'user1' },
    { username: 'user2' },
    { username: 'user3' },
  ];

}
