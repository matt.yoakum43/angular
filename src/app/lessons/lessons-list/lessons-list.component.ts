import { Component, Output, Input, EventEmitter } from '@angular/core';
import { Lesson } from '../../common/models/lesson';

@Component({
  selector: 'app-lessons-list',
  templateUrl: './lessons-list.component.html',
  styleUrls: ['./lessons-list.component.scss']
})
export class LessonsListComponent {
  currentLesson: Lesson;
  @Input() lessons: Lesson[] = [];
  @Output() selected = new EventEmitter();
  @Output() deleted = new EventEmitter();
}
