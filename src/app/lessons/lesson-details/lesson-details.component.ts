import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { Lesson } from '../../common/models/lesson';

@Component({
  selector: 'app-lesson-details',
  templateUrl: './lesson-details.component.html',
  styleUrls: ['./lesson-details.component.scss']
})
export class LessonDetailsComponent {
  currentLesson: Lesson;
  @Input() set lesson(value) {
    if (!value) return;
    this.currentLesson = { ...value };
  }
}
