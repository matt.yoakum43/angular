import { Component, OnInit } from '@angular/core';
import { UsersService } from '../common/services/users.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  users=[];
  selectedUser=null;

  constructor(private usersService: UsersService) { }

  ngOnInit(): void {
    this.users = this.usersService.users;
  }

  selectUser(user) {
    this.selectedUser = user;
  }

}
