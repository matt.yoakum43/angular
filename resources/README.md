Did an angular tutorial learning about all the features. Completed everything in challenges.md Challenges 1-4 were done while following along with the teacher, and 5-6 were done by myself just figuring it out. At this point I could do all of the challenges by myself, and I have a decent understanding of the process of building Angular components

Started from the 00-start branch here:
https://github.com/onehungrymind/angular13-fundamentals-workshop/

`npm start` if you want to boot it up yourself
